import "./App.css";
import PhoneComponent from "./ExPhone/components/PhoneComponent";

function App() {
  return (
    <div className="App">
      <PhoneComponent />
    </div>
  );
}

export default App;
