import { combineReducers } from "redux";
import { phoneReducer } from "./phoneReducer";

export const RootReducer = combineReducers({
  phone: phoneReducer,
});
