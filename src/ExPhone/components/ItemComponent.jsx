import React, { Component } from "react";
import { connect } from "react-redux";
import { VIEW_DETAIL } from "../constant/phoneConstant";

class ItemComponent extends Component {
  render() {
    // console.log(this.props);
    let { hinhAnh, giaBan, tenSP, maSP } = this.props.phone;
    return (
      <div className="col-4 p-4">
        <div className="card border-primary">
          <div className="card-top" style={{ height: "300px" }}>
            <img className="card-img-top" src={hinhAnh} alt="" />
          </div>
          <div className="card-body">
            <h4 className="card-title">{tenSP}</h4>
            <p className="card-text">{giaBan}</p>
            <button
              onClick={() => {
                this.props.handleViewDetail(maSP);
              }}
              className="btn btn-success"
            >
              Xem chi tiết
            </button>
          </div>
        </div>
      </div>
    );
  }
}
let mapDispatchToProps = (dispatch) => {
  return {
    handleViewDetail: (id) => {
      let action = {
        type: VIEW_DETAIL,
        payload: id,
      };
      dispatch(action);
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemComponent);
