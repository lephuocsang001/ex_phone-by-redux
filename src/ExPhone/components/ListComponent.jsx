import React, { Component } from "react";
import { connect } from "react-redux";
import ItemComponent from "./ItemComponent";

class ListComponent extends Component {
  render() {
    // console.log(this.props);
    return (
      <div>
        <div className="row">
          {this.props.data.map((item, index) => {
            return <ItemComponent key={index} phone={item} />;
          })}
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    data: state.phone.data,
  };
};
export default connect(mapStateToProps)(ListComponent);
