import React, { Component } from "react";
import DetailComponent from "./DetailComponent";
import ListComponent from "./ListComponent";

export default class PhoneComponent extends Component {
  render() {
    return (
      <div className="container">
        <ListComponent />
        <DetailComponent />
      </div>
    );
  }
}
